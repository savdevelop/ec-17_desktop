package customUtils.namedEvents;

import java.util.EventObject;

public class NamedEvent extends EventObject {
    private String eventName;
    private String eventInformation;
    private int code;
    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public NamedEvent(Object source, String eventName, String eventInformation, int code) {
        super(source);
        this.eventName = eventName;
        this.eventInformation = eventInformation;
        this.code = code;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventInformation() {
        return eventInformation;
    }

    public int getCode() {
        return code;
    }
}
