package customUtils.namedEvents;

import java.util.EventListener;

public interface NamedEventListener extends EventListener {
    public void namedEventOccurred(NamedEvent e);
}
