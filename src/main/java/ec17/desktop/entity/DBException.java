package ec17.desktop.entity;

public class DBException extends Exception {
    public static final int CONNECTION_ERROR = -1;
    public static final int LOGIN_SUCCESSFUL = 0;
    public static final int LOGIN_WRONG_USERNAME = 1;
    public static final int LOGIN_WRONG_PASSWORD = 2;
    private int errorCode;

    public DBException(String message, int errorCode){
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
