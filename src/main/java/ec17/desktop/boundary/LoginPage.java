package ec17.desktop.boundary;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class LoginPage extends JFrame {
    private class MainPanel extends JPanel {
        public JLabel lblTitle, lblUsername, lblPassword;
        public JTextField txtUsername, txtPassword;
        public JButton btnConfirm;

        public MainPanel() {
            // Set Panel layout settings
            setLayout(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();

            // Set Borders
            setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

            // Initialize Components
            lblTitle    = new JLabel("EC-17");
            lblTitle.setFont(new Font("Serif", Font.ITALIC, 32));
            lblUsername = new JLabel("Username:");
            txtUsername = new JTextField(15);
            lblPassword = new JLabel("Password:");
            txtPassword = new JPasswordField(15);
            btnConfirm = new JButton("Login");

            // Add Components to the panel
            gc.fill = GridBagConstraints.NONE;

            // First Row //
            gc.gridx = 0;
            gc.gridy = 0;
            gc.weightx = 1;
            gc.weighty = 2;
            gc.anchor = GridBagConstraints.PAGE_START;
            gc.insets = new Insets(0,0,20,0);
            add(lblTitle, gc);

            // Second Row //
            gc.gridx = 0;
            gc.gridy++;
            gc.weightx = 1;
            gc.weighty = 0.1;
            gc.anchor = GridBagConstraints.LINE_START;
            gc.insets = new Insets(5,5,5,5);
            add(lblUsername, gc);

            // Third Row //
            gc.gridx = 0;
            gc.gridy++;
            gc.weightx = 1;
            gc.weighty = 0.1;
            gc.anchor = GridBagConstraints.LINE_START;
            gc.insets = new Insets(0,5,5,5);
            add(txtUsername, gc);

            // Fourth Row //
            gc.gridx = 0;
            gc.gridy++;
            gc.weightx = 1;
            gc.weighty = 0.1;
            gc.anchor = GridBagConstraints.LINE_START;
            gc.insets = new Insets(5,5,5,5);
            add(lblPassword, gc);

            // Fifth Row //
            gc.gridx = 0;
            gc.gridy++;
            gc.weightx = 1;
            gc.weighty = 0.1;
            gc.anchor = GridBagConstraints.LINE_START;
            gc.insets = new Insets(0,5,5,5);
            add(txtPassword, gc);

            // Sixth Row //
            gc.gridx = 0;
            gc.gridy++;
            gc.weightx = 1;
            gc.weighty = 2;
            gc.anchor = GridBagConstraints.PAGE_START;
            gc.insets = new Insets(15,5,5,5);
            add(btnConfirm, gc);
        }
    }

    private MainPanel mainPanel;

    public LoginPage() {
        super("Login");

        // Set Frame's content pane
        JPanel contentPane = new JPanel(new GridBagLayout());
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        setContentPane(contentPane);

        // Add components
        mainPanel = new MainPanel();
        add(mainPanel);

        // Set Frame layout
        setPreferredSize(new Dimension(getPreferredSize().width +20,300));
        setLocationRelativeTo(null);

        // Set Frame's behaviour
        setResizable(false);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public String getUsername() {
        return mainPanel.txtUsername.getText();
    }

    public String getPassword() {
        return mainPanel.txtPassword.getText();
    }


    public void addActionListener(ActionListener confirmActionListener) {
        mainPanel.btnConfirm.addActionListener(confirmActionListener);
    }

    public void showError(String title, String message){
        JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public void close(){
        mainPanel.txtPassword.setText("");
        this.setVisible(false);
    }

    public void activate(){
        this.setVisible(true);
    }
}
