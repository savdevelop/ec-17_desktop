package ec17.desktop.boundary;

import customUtils.namedEvents.NamedEvent;
import customUtils.namedEvents.NamedEventListener;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

interface TabListener {
    public void changeTab();
}

class ListData {
    private String name = "";
    private String objectId = "";


    public ListData(String name, String objectId) {
        this.name = name;
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public String getObjectId() {
        return objectId;
    }

    @Override
    public String toString(){
        return name;
    }
}

public class StatisticsPage extends JFrame {
    public static final int TABS_CHANGED = 0;

    /* -- Global Filters Listeners -- */
    // Customers Panel Listeners
    public static final int CUSTOMER_TOP_REVIEWERS = 1;
    public static final int CUSTOMER_TOP_BUYERS = 2;
    public static final int CUSTOMER_GENDER = 3;
    public static final int CUSTOMER_AVG_AGE = 4;

    // Products Panel Listeners
    public static final int PRODUCTS_TOP_SELLER = 5;
    public static final int PRODUCTS_TOP_RATED = 6;
    public static final int PRODUCTS_TOP_INCOME = 7;

    // Categories Panel Listeners
    public static final int CATEGORIES_TOP_SELLER = 8;
    public static final int CATEGORIES_SUB_TOP_SELLER = 9;
    public static final int CATEGORIES_TOP_INCOME = 10;
    public static final int CATEGORIES_SUB_TOP_INCOME = 11;

    /* -- Specific Filters Listeners -- */
    // Customer Panel Listener
    public static final int CUSTOMER_REVIEW_NUMBER = 0;
    public static final int CUSTOMER_BOUGHT_PRODUCTS = 1;
    // Product Panel Listener
    public static final int SPECIFIC_PRODUCTS_TOP_SELLER = 2;
    public static final int SPECIFIC_PRODUCTS_TOP_RATING = 3;
    public static final int SPECIFIC_PRODUCTS_TOP_INCOME = 4;
    // Category Panel Listener
    public static final int SPECIFIC_CATEGORY_TOP_SELLER = 5;
    public static final int SPECIFIC_CATEGORY_TOP_RATING = 6;
    public static final int SPECIFIC_CATEGORY_TOP_INCOME = 7;

    // Monitoring Panel Listeners
    public static final int MONITORING_PANEL_ADD_BTN = 0;
    public static final int MONITORING_PANEL_DEL_BTN = 1;
    public static final int MONITORING_PANEL_SEARCH_BTN_CUSTOMERS=2;
    public static final int MONITORING_PANEL_SEARCH_BTN_PRODUCTS=3;
    public static final int MONITORING_PANEL_SEARCH_BTN_CATEGORIES=4;
    public static final int MONITORING_PANEL_MONITOR_LIST_UPDATED=5;


    // Chart Type
    public static final int CHART_BARCHART = 0;
    public static final int CHART_PIECHART = 1;

    private class TabMenu extends JPanel {
        private JToolBar toolbar;

        public TabMenu() {
            toolbar = new JToolBar("Statistics Menu");
            toolbar.setFloatable(false);

            // Set Panel layout settings
            setLayout(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();

            // Add Components to the panel
            gc.fill = GridBagConstraints.NONE;

            // First Row //
            gc.gridx = 0;
            gc.gridy = 0;
            gc.anchor = GridBagConstraints.PAGE_START;
            gc.insets = new Insets(5, 0, 5, 0);
            add(toolbar, gc);

            // Set Borders
            setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        }

        public Component[] getTabs() {
            return toolbar.getComponents();
        }

        public String getTabName(int index) {
            return ((Button) toolbar.getComponent(index)).getLabel();
        }

        public void addTab(Button button) {
            toolbar.add(button);
        }

        public int getLength() {
            return toolbar.getComponentCount();
        }
    }

    private class StatisticsPanel extends JPanel {

        private class FiltersGroup extends JPanel {
            private List<JRadioButton> filtersList = new ArrayList<JRadioButton>();
            private ButtonGroup filtersGroup = new ButtonGroup();
            private JLabel title;
            GridBagConstraints gc;

            FiltersGroup(String title) {
                setLayout(new GridBagLayout());
                gc = new GridBagConstraints();
                //setPreferredSize(new Dimension(100,300));
                setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10),
                        BorderFactory.createEtchedBorder()));
                this.title = new JLabel(title);
                this.title.setFont(new Font("Serif", Font.ITALIC, 16));

                gc.gridx = 0;
                gc.gridy = 0;
                gc.weightx = 1;
                gc.weighty = 0.01;
                gc.anchor = GridBagConstraints.FIRST_LINE_START;
                gc.insets = new Insets(0, 10, 10, 0);
                gc.fill = GridBagConstraints.HORIZONTAL;
                add(this.title, gc);
                gc.gridy++;
                gc.insets = new Insets(0, 10, 0, 0);
            }

            public void addFilter(JRadioButton filter) {
                filtersList.add(filter);
                filtersGroup.add(filter);
                add(filter, gc);
                gc.gridy++;
            }

            public void removeFilter(JRadioButton filter) {
                filtersList.remove(filter);
                filtersGroup.remove(filter);
                gc.gridy--;
            }

        }

        private class MonitoringPanel extends JPanel {
            public DefaultListModel<ListData> searchedResultModel;
            public JList searchedResultList;
            public DefaultListModel<ListData> monitoringItemsModel;
            public JList monitoringItemsList;
            public JTextField txtSearchedResultBox;
            public JButton btnSearch;
            public JButton btnSelectItem;
            public JButton btnRemoveItem;

            public MonitoringPanel(String title) {

                setLayout(new GridBagLayout());
                GridBagConstraints gc = new GridBagConstraints();
                // Set Borders
                setBorder(BorderFactory.createTitledBorder(title));
                //setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

                txtSearchedResultBox = new JTextField(13);
                btnSearch = new JButton("Search");
                ;
                btnRemoveItem = new JButton("<");

                Dimension listDimension = new Dimension(250, 100);
                searchedResultModel = new DefaultListModel<ListData>();
                searchedResultList = new JList();
                searchedResultList.setModel(searchedResultModel);
                searchedResultList.setPreferredSize(listDimension);
                searchedResultList.setMaximumSize(listDimension);
                searchedResultList.setBorder(BorderFactory.createEtchedBorder());

                btnSelectItem = new JButton(">");

                monitoringItemsModel = new DefaultListModel();
                monitoringItemsList = new JList();
                monitoringItemsList.setModel(monitoringItemsModel);
                monitoringItemsList.setPreferredSize(listDimension);
                monitoringItemsList.setMaximumSize(listDimension);
                monitoringItemsList.setBorder(BorderFactory.createEtchedBorder());

                gc.fill = GridBagConstraints.NONE;

                // First ROW
                gc.gridx = 0;
                gc.gridy = 0;

                gc.weightx = 1;
                gc.weighty = .1;
                gc.anchor = GridBagConstraints.LAST_LINE_END;
                gc.fill = GridBagConstraints.NONE;
                gc.insets = new Insets(0, 0, 2, 0);
                add(txtSearchedResultBox, gc);

                gc.gridx++;
                gc.weightx = 0.0;
                gc.weighty = .1;
                gc.anchor = GridBagConstraints.LAST_LINE_START;
                gc.fill = GridBagConstraints.NONE;
                gc.insets = new Insets(0, 0, 1, 0);
                add(btnSearch, gc);

                // NEXT ROW
                gc.gridy++;
                gc.gridx = 0;

                gc.insets = new Insets(0, 0, 0, 0);
                gc.gridwidth = 2;
                gc.gridheight = 2;
                gc.weightx = 1;
                gc.weighty = 1;
                gc.anchor = GridBagConstraints.FIRST_LINE_END;
                gc.fill = GridBagConstraints.VERTICAL;
                add(searchedResultList, gc);
                gc.gridx++;
                gc.gridwidth = 1;

                gc.gridx++;
                gc.weightx = 0.1;
                gc.weighty = 1;
                gc.gridheight = 1;
                gc.anchor = GridBagConstraints.PAGE_END;
                gc.fill = GridBagConstraints.NONE;
                gc.insets = new Insets(0, 15, 0, 15);
                add(btnSelectItem, gc);
                gc.insets = new Insets(0, 0, 0, 0);

                gc.gridy++;
                gc.weightx = 0.1;
                gc.weighty = 1;
                gc.gridheight = 1;
                gc.anchor = GridBagConstraints.PAGE_START;
                gc.fill = GridBagConstraints.NONE;
                gc.insets = new Insets(0, 15, 15, 15);
                add(btnRemoveItem, gc);
                gc.insets = new Insets(0, 0, 0, 0);
                gc.gridy--;

                gc.gridx++;
                gc.gridheight = 2;
                gc.weightx = 0.5;
                gc.weighty = 1;
                gc.anchor = GridBagConstraints.FIRST_LINE_START;
                gc.fill = GridBagConstraints.VERTICAL;
                add(monitoringItemsList, gc);
                gc.gridheight = 1;
            }
        }

        DefaultCategoryDataset specificChartModel = new DefaultCategoryDataset();
        JFreeChart specificChart;
        ChartPanel specificChartPane;

        DefaultCategoryDataset globalChartModel = new DefaultCategoryDataset();
        JFreeChart globalChart;
        ChartPanel globalChartPane;

        private JLabel lblTitle;
        public FiltersGroup specificFilters = new FiltersGroup("Compare");
        public FiltersGroup globalFilters = new FiltersGroup("Global Filters");
        MonitoringPanel monitoringPanel;

        public StatisticsPanel(String title) {
            setLayout(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();

            specificChart = ChartFactory.createBarChart(
                    "",
                    "",
                    "",
                    specificChartModel,
                    PlotOrientation.VERTICAL,
                    false, true, false);

            globalChart = ChartFactory.createBarChart(
                    "",
                    "",
                    "",
                    globalChartModel,
                    PlotOrientation.VERTICAL,
                    false, true, false);

            specificChartPane = new ChartPanel(specificChart);
            globalChartPane = new ChartPanel(globalChart);


            // Set Borders
            setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

            lblTitle = new JLabel(title);

            // Create components

            JSeparator vSeparator = new JSeparator(JSeparator.VERTICAL);
            vSeparator.setPreferredSize(new Dimension(5, 1));

            monitoringPanel = new MonitoringPanel("Manage " + title.toLowerCase() + " to monitor");
            //monitoringPanel.setPreferredSize(new Dimension(300, 300));
            monitoringPanel.setMinimumSize(new Dimension(getPreferredSize().width, 300));
            Dimension d = new Dimension(250, getPreferredSize().height);

            specificFilters.setMinimumSize(d);
            specificFilters.setMaximumSize(d);
            globalFilters.setMinimumSize(d);
            globalFilters.setMaximumSize(d);

            // Add Components to the panel
            gc.insets = new Insets(0, 0, 0, 0);

            gc.anchor = GridBagConstraints.CENTER;
            gc.gridx = 0;
            gc.gridy = 0;

            // FIRST ROW
            gc.weightx = 0.1;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.FIRST_LINE_START;
            gc.insets = new Insets(5, 5, 5, 5);
            gc.fill = GridBagConstraints.VERTICAL;
            add(specificFilters, gc);

            gc.gridx++;
            gc.weightx = 1;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.FIRST_LINE_START;
            gc.fill = GridBagConstraints.BOTH;
            add(specificChartPane, gc);

            gc.gridx++;
            gc.weightx = 0;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.CENTER;
            gc.fill = GridBagConstraints.VERTICAL;
            gc.insets = new Insets(0, 5, 0, 5);
            add(vSeparator, gc);
            gc.insets = new Insets(0, 0, 0, 0);


            gc.gridx++;
            gc.weightx = 1;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.FIRST_LINE_START;
            gc.fill = GridBagConstraints.BOTH;
            add(globalChartPane, gc);

            gc.gridx++;
            gc.weightx = 0.1;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.FIRST_LINE_START;
            gc.insets = new Insets(5, 5, 5, 5);
            gc.fill = GridBagConstraints.VERTICAL;
            add(globalFilters, gc);

            // NEXT ROW
            gc.gridx = 0;
            gc.gridy++;

            gc.weightx = 0;
            gc.weighty = 0.1;
            gc.gridwidth = 2;
            gc.anchor = GridBagConstraints.CENTER;
            gc.fill = GridBagConstraints.HORIZONTAL;
            gc.insets = new Insets(0, 0, 20, 0);
            add(monitoringPanel, gc);
            gc.insets = new Insets(0, 0, 0, 0);
            gc.gridwidth = 0;
        }

        public String getTitle() {
            return lblTitle.getText();
        }
    }

    private TabMenu tabMenu;
    private List<StatisticsPanel> panels;
    private StatisticsPanel currentPanel;
    private NamedEventListener statisticsListener;

    public StatisticsPage() {
        super("Statistics");
        // Set Frame's content pane
        JPanel content = new JPanel(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        content.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(content);

        // Create components
        tabMenu = new TabMenu();
        tabMenu.addTab(new Button("Customers"));
        tabMenu.addTab(new Button("Products"));
        tabMenu.addTab(new Button("Categories"));

        panels = new ArrayList<StatisticsPanel>();

        StatisticsPanel customersPanel = new StatisticsPanel("Customers");
        StatisticsPanel productsPanel = new StatisticsPanel("Products");
        StatisticsPanel categoriesPanel = new StatisticsPanel("Categories");
        panels.add(customersPanel);
        panels.add(productsPanel);
        panels.add(categoriesPanel);


        /* Adding Specific Filters */

        /* -- Customers -- */
        addSpecificFilter("Review Number", customersPanel, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    NamedEvent ev = new NamedEvent(this, "SpecificFilterPressed", "CUSTOMER_REVIEW_NUMBER", StatisticsPage.CUSTOMER_REVIEW_NUMBER);
                    statisticsListener.namedEventOccurred(ev);
                }
            });
        addSpecificFilter("Bought Prodocuts", customersPanel, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    NamedEvent ev = new NamedEvent(this, "SpecificFilterPressed", "CUSTOMER_BOUGHT_PRODUCTS", CUSTOMER_BOUGHT_PRODUCTS);
                    statisticsListener.namedEventOccurred(ev);
                }
            });

        /* -- Products -- */
        addSpecificFilter("Most Bought", productsPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "SpecificFilterPressed", "SPECIFIC_PRODUCTS_TOP_SELLER", SPECIFIC_PRODUCTS_TOP_SELLER);
                statisticsListener.namedEventOccurred(ev);
            }
        });
        addSpecificFilter("Best Rated", productsPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "SpecificFilterPressed", "SPECIFIC_PRODUCTS_TOP_RATING", SPECIFIC_PRODUCTS_TOP_RATING);
                statisticsListener.namedEventOccurred(ev);
            }
        });
        addSpecificFilter("Most Income", productsPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "SpecificFilterPressed", "SPECIFIC_PRODUCTS_TOP_INCOME", SPECIFIC_PRODUCTS_TOP_INCOME);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addSpecificFilter("Most Bought", categoriesPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "SpecificFilterPressed", "SPECIFIC_CATEGORY_TOP_SELLER", SPECIFIC_CATEGORY_TOP_SELLER);
                statisticsListener.namedEventOccurred(ev);
            }
        });
/*        addSpecificFilter("Best Rated", categoriesPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "SpecificFilterPressed", "SPECIFIC_CATEGORY_TOP_RATING", SPECIFIC_CATEGORY_TOP_RATING);
                statisticsListener.namedEventOccurred(ev);
            }
        });*/
        addSpecificFilter("Most Income", categoriesPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "SpecificFilterPressed", "SPECIFIC_CATEGORY_TOP_INCOME", SPECIFIC_CATEGORY_TOP_INCOME);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        /* Adding Global Filters */
        addGlobalFilter("TopReviewers", customersPanel,  new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "TOP_REVIEWERS", StatisticsPage.CUSTOMER_TOP_REVIEWERS);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addGlobalFilter("TopCustomers", customersPanel,  new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "TOP_CUSTOMERS", StatisticsPage.CUSTOMER_TOP_BUYERS);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addGlobalFilter("Customer Gender", customersPanel,  new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "CUSTOMER_GENDER", StatisticsPage.CUSTOMER_GENDER);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addGlobalFilter("Average Age", customersPanel,  new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "CUSTOMER_AVG_AGE", StatisticsPage.CUSTOMER_AVG_AGE);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        /* -- Products -- */
        addGlobalFilter("Top Sellers", productsPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "PRODUCTS_TOP_SELLER", StatisticsPage.PRODUCTS_TOP_SELLER);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addGlobalFilter("Top Rated", productsPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "PRODUCTS_TOP_RATED", StatisticsPage.PRODUCTS_TOP_RATED);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addGlobalFilter("Top Income", productsPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "PRODUCTS_TOP_INCOME", StatisticsPage.PRODUCTS_TOP_INCOME);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        /* -- Categories -- */
        addGlobalFilter("Top Categ.", categoriesPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "CATEGORIES_TOP_SELLER", StatisticsPage.CATEGORIES_TOP_SELLER);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addGlobalFilter("Top Sub-Categ.", categoriesPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "CATEGORIES_SUB_TOP_SELLER", StatisticsPage.CATEGORIES_SUB_TOP_SELLER);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addGlobalFilter("Top Categ. Income", categoriesPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "CATEGORIES_TOP_INCOME", StatisticsPage.CATEGORIES_TOP_INCOME);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        addGlobalFilter("Top Sub-Categ. Income", categoriesPanel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "GlobalFilterPressed", "CATEGORIES_SUB_TOP_INCOME", StatisticsPage.CATEGORIES_SUB_TOP_INCOME);
                statisticsListener.namedEventOccurred(ev);
            }
        });


        /* -- Monitoring Panel Listeners -- */
        addMonitoringListener(customersPanel);
        addMonitoringListener(productsPanel);
        addMonitoringListener(categoriesPanel);

        // Tab Menu
        gc.fill = GridBagConstraints.NONE;
        gc.gridx = 0;
        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = .01;
        gc.anchor = GridBagConstraints.PAGE_START;
        add(tabMenu, gc);

        // Panels
        gc.fill = GridBagConstraints.BOTH;
        gc.gridx = 0;
        gc.gridy = 1;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.PAGE_START;
        for (StatisticsPanel panel : panels) {
            panel.setVisible(false);
            add(panel, gc);
        }
        showTab(0);
        // Set Frame layout
        setPreferredSize(new Dimension(1300, 800));
        setMinimumSize(new Dimension(1300, 800));
        setLocationRelativeTo(null);

        // Set Frame's behaviour
        pack();
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    public void activate() {
        if (!isVisible()) {
            setVisible(true);
        }
    }

    public void setStatisticsListener(NamedEventListener listener) {
        this.statisticsListener = listener;
    }

    public void addTabsActionListener(ActionListener actionListener) {
        for (Component tab : this.tabMenu.getTabs()) {
            ((Button) tab).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    NamedEvent ev = new NamedEvent(this, "TABS", e.getActionCommand(), StatisticsPage.TABS_CHANGED);
                    statisticsListener.namedEventOccurred(ev);
                }
            });
        }
    }

    // TODO: Gestire con exception ?
    public boolean showTab(int index) {
        boolean tabChanged = false;
        if (index >= 0 && index < tabMenu.getLength()) {
            if (currentPanel != null) {
                currentPanel.setVisible(false);
            }
            currentPanel = panels.get(index);
            currentPanel.setVisible(true);
            tabChanged = true;
        }
        return tabChanged;
    }

    // TODO: Convertire in  Hashmap ?
    public boolean showTab(String tabName) {
        boolean tabChanged = false;
        for (int i = 0; i <= tabMenu.getLength(); i++) {
            if (tabName.equals(tabMenu.getTabName(i))) {
                tabChanged = showTab(i);
                break;
            }
        }
        return tabChanged;
    }

    // Global Chart as BAR CHART
    public void setGlobalChartModel(String title, String categoryAxis, String valueAxis, DefaultCategoryDataset dataset, int chartType) {

        if (chartType == StatisticsPage.CHART_BARCHART) {
            JFreeChart newChart = ChartFactory.createBarChart(
                    title,
                    categoryAxis,
                    valueAxis,
                    (CategoryDataset) dataset,
                    PlotOrientation.HORIZONTAL,
                    false, true, false);
            currentPanel.globalChartPane.setChart(newChart);
        }
    }

    // Global Chart as PIE CHART
    public void setGlobalChartModel(String title, DefaultPieDataset dataset, int chartType) {
        if (chartType == StatisticsPage.CHART_PIECHART) {
            JFreeChart newChart = ChartFactory.createPieChart(
                    title,
                    dataset,
                    true, true, false);
            currentPanel.globalChartPane.setChart(newChart);
        }
    }

    // Global Chart as BAR CHART
    public void setSpecificChartModel(String title, String categoryAxis, String valueAxis, DefaultCategoryDataset dataset, int chartType) {

        if (chartType == StatisticsPage.CHART_BARCHART) {
            JFreeChart newChart = ChartFactory.createBarChart(
                    title,
                    categoryAxis,
                    valueAxis,
                    (CategoryDataset) dataset,
                    PlotOrientation.HORIZONTAL,
                    false, true, false);
            currentPanel.specificChartPane.setChart(newChart);
        }
    }

    // Global Chart as PIE CHART
    public void setSpecificChartModel(String title, DefaultPieDataset dataset, int chartType) {
        if (chartType == StatisticsPage.CHART_PIECHART) {
            JFreeChart newChart = ChartFactory.createPieChart(
                    title,
                    dataset,
                    true, true, false);
            currentPanel.specificChartPane.setChart(newChart);
        }
    }

    public void showError(String title, String message) {
        JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public void addGlobalFilter(String title, StatisticsPanel panel, ActionListener e){
        JRadioButton gFilter = new JRadioButton(title);
        gFilter.addActionListener(e);
        panel.globalFilters.addFilter(gFilter);
    }

    public void addSpecificFilter(String title, StatisticsPanel panel, ActionListener e){
        JRadioButton sFilter = new JRadioButton(title);
        sFilter.addActionListener(e);
        panel.specificFilters.addFilter(sFilter);
    }

    public void addMonitoringListener(StatisticsPanel panel){

        panel.monitoringPanel.btnSelectItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "MonitorEvent", "MONITORING_PANEL_ADD_BTN", StatisticsPage.MONITORING_PANEL_ADD_BTN);
                statisticsListener.namedEventOccurred(ev);
            }
        });
        panel.monitoringPanel.btnRemoveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NamedEvent ev = new NamedEvent(this, "MonitorEvent", "MONITORING_PANEL_DEL_BTN", StatisticsPage.MONITORING_PANEL_DEL_BTN);
                statisticsListener.namedEventOccurred(ev);
            }
        });

        panel.monitoringPanel.btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String eventInfo = "";
                int eventCode = -1;
                if(currentPanel.getTitle().equals("Customers")) {
                    eventInfo = "MONITORING_PANEL_SEARCH_BTN_CUSTOMERS";
                    eventCode = MONITORING_PANEL_SEARCH_BTN_CUSTOMERS;
                }else if(currentPanel.getTitle().equals("Products")){
                    eventInfo = "MONITORING_PANEL_SEARCH_BTN_PRODUCTS";
                    eventCode = MONITORING_PANEL_SEARCH_BTN_PRODUCTS;
                }else if(currentPanel.getTitle().equals("Categories")){
                    eventInfo = "MONITORING_PANEL_SEARCH_BTN_CATEGORIES";
                    eventCode = MONITORING_PANEL_SEARCH_BTN_CATEGORIES;
                }
                NamedEvent ev = new NamedEvent(this, "MonitorEvent", eventInfo, eventCode);
                statisticsListener.namedEventOccurred(ev);
            }
        });
    }

    public void addItemToMonitoring(){
        if(currentPanel.monitoringPanel.monitoringItemsModel.getSize() <= 10) {
            ListData selectedValue = (ListData) currentPanel.monitoringPanel.searchedResultList.getSelectedValue();
            if(selectedValue != null) {
                currentPanel.monitoringPanel.monitoringItemsModel.addElement(selectedValue);
                if (currentPanel.monitoringPanel.monitoringItemsModel.getSize() == 10) {
                    currentPanel.monitoringPanel.btnSelectItem.setEnabled(false);
                }
                NamedEvent ev = new NamedEvent(this, "MonitorEvent", "MONITORING_PANEL_MONITOR_LIST_UPDATED", MONITORING_PANEL_MONITOR_LIST_UPDATED);
                statisticsListener.namedEventOccurred(ev);
            }
        }
    }

    public void removeItemFromMonitoring(){
        int selectedValue = currentPanel.monitoringPanel.monitoringItemsList.getSelectedIndex();
        if(selectedValue >= 0 && selectedValue <= currentPanel.monitoringPanel.monitoringItemsModel.getSize()) {
            currentPanel.monitoringPanel.monitoringItemsModel.remove(selectedValue);
            if (!currentPanel.monitoringPanel.btnSelectItem.isEnabled()) {
                currentPanel.monitoringPanel.btnSelectItem.setEnabled(true);
            }
            NamedEvent ev = new NamedEvent(this, "MonitorEvent", "MONITORING_PANEL_MONITOR_LIST_UPDATED", MONITORING_PANEL_MONITOR_LIST_UPDATED);
            statisticsListener.namedEventOccurred(ev);
        }
    }

    public String getSearchString(){
        return currentPanel.monitoringPanel.txtSearchedResultBox.getText();
    }

    public void setSearchResultList(List<Map<String,String>> items){
        DefaultListModel<ListData> searchedResultModel = new DefaultListModel<ListData>();
        for (Map<String, String> item : items) {
            searchedResultModel.addElement(new ListData(item.get("name"), item.get("objectId")));
        }
        currentPanel.monitoringPanel.searchedResultList.setModel(searchedResultModel);
    }

    public List<Map<String,String>> getMonitoringList(){
        List<Map<String,String>> resultList = new ArrayList<>();
        DefaultListModel<ListData> listModel = currentPanel.monitoringPanel.monitoringItemsModel;

        for(int i=0; i<listModel.size(); i++){
            Map<String,String> resultMap = new HashMap<>();
            ListData data = listModel.get(i);
            resultMap.put("name", data.getName());
            resultMap.put("objectId", data.getObjectId());
            resultList.add(resultMap);
        }

        return resultList;
    }
}

