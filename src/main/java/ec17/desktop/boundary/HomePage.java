package ec17.desktop.boundary;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class HomePage extends JFrame {
    private class MainPanel extends JPanel{
        private JLabel lblTitle;
        private JButton btnItemManager, btnCustomerManager, btnCategoryManager, btnStatistics;

        MainPanel() {
            // Set layout Settings
            setLayout(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();
            // Set Borders
            setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

            // Initialize Components
            lblTitle = new JLabel("EC-17");
            lblTitle.setFont(new Font("Serif", Font.ITALIC, 72));
            btnItemManager = new JButton("Item Manager");
            btnCustomerManager = new JButton("Customer Manager");
            btnCategoryManager = new JButton("Category Manager");
            btnStatistics = new JButton("Statistics");

            // Add Components to the panel

            JPanel box = new JPanel(new GridLayout(2, 2,10,10));
            box.add(btnItemManager);
            box.add(btnCustomerManager);
            box.add(btnCategoryManager);
            box.add(btnStatistics);
            box.setPreferredSize(new Dimension(400,400));

            // ROW 1
            gc.fill = GridBagConstraints.NONE;
            gc.gridx = 0;
            gc.gridy = 0;
            gc.weightx = 1;
            gc.weighty = 0.2;
            gc.anchor = GridBagConstraints.PAGE_START;
            gc.insets = new Insets(10,10,40,10);
            add(lblTitle, gc);

            // ROW 2
            gc.gridx = 0;
            gc.gridy = 1;
            gc.weightx = 1;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.PAGE_START;
            gc.insets = new Insets(10,10,10,10);
            add(box,gc);
        }
    }

    private MainPanel mainPanel;

    public HomePage(){
        super("HomePage");

        // Set Frame's content pane
        JPanel content = new JPanel(new GridLayout());
        content.setBorder(new EmptyBorder(20, 20, 20, 20));
        setContentPane(content);

        // Add components
        mainPanel = new MainPanel();
        add(mainPanel);

        // Set Frame layout
        setPreferredSize(new Dimension(800, 800));
        setMinimumSize(new Dimension(480, 650));
        setLocationRelativeTo(null);

        // Set Frame's behaviour
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void activate(){
        this.setVisible(true);
    }

    public void addActionListenerStatistics(ActionListener actionListener){
        mainPanel.btnStatistics.addActionListener(actionListener);
    }
}
