package ec17.desktop.control;

import ec17.desktop.boundary.LoginPage;
import ec17.desktop.entity.DBException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginController implements ActionListener {
    private LoginPage loginPage;
    private HomePageController homePageController;
    private BackendInterface database = BackendInterface.getInstance();

    public LoginController(LoginPage loginPage, HomePageController homePageController) {
        loginPage.addActionListener(this);
        this.loginPage = loginPage;
        this.homePageController = homePageController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (database.checkLogin(loginPage.getUsername(), loginPage.getPassword())) {
                loginPage.close();
                if (homePageController != null) {
                    homePageController.loginPerformed();
                } else {
                    loginPage.showError("Internal Error", "homePage is not set.");
                }
            }
        } catch (DBException ex) {
            loginPage.showError("Login Error", ex.getMessage());
        }
    }

    public void activate() {
        //TODO: re-activate login, de-activate auto-login
        //loginPage.activate();
        homePageController.loginPerformed();
    }

    public void destroy() {
        loginPage.dispose();
    }
}


