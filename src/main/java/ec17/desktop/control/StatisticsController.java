package ec17.desktop.control;

import customUtils.namedEvents.NamedEvent;
import customUtils.namedEvents.NamedEventListener;
import ec17.desktop.boundary.StatisticsPage;
import ec17.desktop.entity.DBException;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

public class StatisticsController implements ActionListener {
    private StatisticsPage statisticsPage;
    private BackendInterface database;


    public StatisticsController(StatisticsPage statisticsPage) {
        database = BackendInterface.getInstance();
        this.statisticsPage = statisticsPage;
        statisticsPage.addTabsActionListener(this);
        statisticsPage.setStatisticsListener(new NamedEventListener() {
            @Override
            public void namedEventOccurred(NamedEvent e) {
                try {
                    if (e.getEventName().equals("TABS")) {
                        statisticsPage.showTab(e.getEventInformation());
                    } else if (e.getEventName().equals("MonitorEvent")) {
                        switch (e.getCode()){
                            case StatisticsPage.MONITORING_PANEL_SEARCH_BTN_CUSTOMERS:
                                statisticsPage.setSearchResultList(database.getCustomersByString(statisticsPage.getSearchString()));
                                break;
                            case StatisticsPage.MONITORING_PANEL_SEARCH_BTN_PRODUCTS:
                                statisticsPage.setSearchResultList(database.getProductsByString(statisticsPage.getSearchString()));
                                break;
                            case StatisticsPage.MONITORING_PANEL_SEARCH_BTN_CATEGORIES:
                                statisticsPage.setSearchResultList(database.getCategoryByString(statisticsPage.getSearchString()));
                                break;
                            case StatisticsPage.MONITORING_PANEL_ADD_BTN:
                                statisticsPage.addItemToMonitoring();
                                break;
                            case StatisticsPage.MONITORING_PANEL_DEL_BTN:
                                statisticsPage.removeItemFromMonitoring();
                                break;
                            case StatisticsPage.MONITORING_PANEL_MONITOR_LIST_UPDATED:
                                System.out.println(e.getEventInformation());
                                break;
                        }
                    } else if (e.getEventName().equals("SpecificFilterPressed")) {
                        switch(e.getCode()) {
                            case StatisticsPage.CUSTOMER_REVIEW_NUMBER:
                                    updateSpecificChart(database.getReviewNumber(statisticsPage.getMonitoringList()),
                                        "Top Reviewers", "user", "reviews",
                                        StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.CUSTOMER_BOUGHT_PRODUCTS:
                                updateSpecificChart(database.getBoughtProducts(statisticsPage.getMonitoringList()),
                                        "Most Bought", "user", "buy",
                                        StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.SPECIFIC_PRODUCTS_TOP_SELLER:
                                updateSpecificChart(database.getTopProducts(statisticsPage.getMonitoringList()),
                                        "Top Product", "products", "sold",
                                        StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.SPECIFIC_PRODUCTS_TOP_RATING:
                                updateSpecificChart(database.getTopRatedProducts(statisticsPage.getMonitoringList()),
                                        "Top Rating", "products", "rating",
                                        StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.SPECIFIC_PRODUCTS_TOP_INCOME:
                                updateSpecificChart(database.getTopIncomeProducts(statisticsPage.getMonitoringList()),
                                        "Top Income", "products", "EUR",
                                        StatisticsPage.CHART_BARCHART);
                                break;

                            case StatisticsPage.SPECIFIC_CATEGORY_TOP_INCOME:
                                updateSpecificChart(database.getTopIncomeCategory(statisticsPage.getMonitoringList()),
                                        "Top Income", "Categories", "EUR",
                                        StatisticsPage.CHART_BARCHART);
                                break;

                            case StatisticsPage.SPECIFIC_CATEGORY_TOP_SELLER:
                                updateSpecificChart(database.getTopCategory(statisticsPage.getMonitoringList()),
                                        "Top Rating", "Categories", "cash",
                                        StatisticsPage.CHART_BARCHART);
                                break;
                        }
                    } else if (e.getEventName().equals("GlobalFilterPressed")) {
                        switch (e.getCode()) {
                            case StatisticsPage.CUSTOMER_TOP_REVIEWERS:
                                updateGlobalChart(database.getCustomerTopReviewers(),
                                        "Top Reviewers", "user", "reviews",
                                        StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.CUSTOMER_TOP_BUYERS:
                                updateGlobalChart(database.getCustomerTopBuyers(), "Top Customers",
                                        "user", "Bought Products",
                                        StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.CUSTOMER_GENDER:
                                updateGlobalChart(database.getCustomerGenderStats(), "Customer Gender", StatisticsPage.CHART_PIECHART);
                                break;
                            case StatisticsPage.CUSTOMER_AVG_AGE:
                                updateGlobalChart(database.getCustomerAvgAge(), "Average Age",
                                        "Age Range", "Users Number",
                                        StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.PRODUCTS_TOP_SELLER:
                                updateGlobalChart(database.getProductTopSellers(), "Top Seller",
                                        "Products", "Products Numbers", StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.PRODUCTS_TOP_RATED:
                                updateGlobalChart(database.getProductTopRated(), "Top Rated",
                                        "Products", "Avg. Rating", StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.PRODUCTS_TOP_INCOME:
                                updateGlobalChart(database.getProductTopIncome(), "Top Income",
                                        "Products", "Total Income", StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.CATEGORIES_TOP_SELLER:
                                updateGlobalChart(database.getCategoryTopSeller(), "Top Seller",
                                        "Categories", "Products Sold", StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.CATEGORIES_SUB_TOP_SELLER:
                                updateGlobalChart(database.getCategorySubTopSeller(), "Top Seller",
                                        "Sub Categories", "Products Sold", StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.CATEGORIES_TOP_INCOME:
                                updateGlobalChart(database.getCategoryTopIncome(), "Top Seller",
                                        "Sub Categories", "Products Sold", StatisticsPage.CHART_BARCHART);
                                break;
                            case StatisticsPage.CATEGORIES_SUB_TOP_INCOME:
                                updateGlobalChart(database.getCategorySubTopIncome(), "Top Seller",
                                        "Sub Categories", "Products Sold", StatisticsPage.CHART_BARCHART);
                                break;
                        }
                    }
                } catch (DBException ex) {
                    statisticsPage.showError("DatabaseError", ex.getMessage());
                }
            }
        });
    }

    private void updateGlobalChart (List<Map<String,String>> statistics, String title, String xName, String yName, int chartType ){

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (Map<String,String> statistic: statistics) {
            String value = statistic.get("value");

            dataset.setValue(Double.parseDouble(value), xName, statistic.get("key"));
        }
        statisticsPage.setGlobalChartModel(title, xName, yName, dataset, chartType);
    }

    private void updateGlobalChart (List<Map<String,String>> statistics, String title, int chartType ){
        DefaultPieDataset dataset = new DefaultPieDataset();

        for (Map<String,String> statistic: statistics) {
            dataset.setValue(statistic.get("key"), Integer.parseInt(statistic.get("value")));
        }
        statisticsPage.setGlobalChartModel(title, dataset, chartType);
    }

    private void updateSpecificChart (List<Map<String,String>> statistics, String title, String xName, String yName, int chartType ){

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (Map<String,String> statistic: statistics) {
            String value = statistic.get("value");

            dataset.setValue(Double.parseDouble(value), xName, statistic.get("key"));
        }
        statisticsPage.setSpecificChartModel(title, xName, yName, dataset, chartType);
    }

    private void updateSpecificChart (List<Map<String,String>> statistics, String title, int chartType ){
        DefaultPieDataset dataset = new DefaultPieDataset();

        for (Map<String,String> statistic: statistics) {
            dataset.setValue(statistic.get("key"), Integer.parseInt(statistic.get("value")));
        }
        statisticsPage.setSpecificChartModel(title, dataset, chartType);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        statisticsPage.showTab(e.getActionCommand());
    }

    public void showStatistics () {
        statisticsPage.activate();
    }
}
