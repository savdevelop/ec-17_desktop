package ec17.desktop.control;

import ec17.desktop.boundary.HomePage;
import ec17.desktop.boundary.StatisticsPage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HomePageController implements ActionListener {
    private HomePage homePage;
    private LoginController loginController;
    private StatisticsController statisticsController;

    public HomePageController(HomePage homePage, StatisticsController statisticsController) {
        this.homePage = homePage;
        homePage.addActionListenerStatistics(this);
        this.statisticsController = statisticsController;
    }

    public void setLoginController(LoginController loginController){
        this.loginController = loginController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Statistics")){
            statisticsController.showStatistics();
        }
    }

    public void loginPerformed (){
        homePage.activate();
        loginController.destroy();
    }

}
