package ec17.desktop.control;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.JsonNode;
import ec17.desktop.entity.DBException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;


import java.io.IOException;
import java.util.*;

class ScoreObjectId {

    public static final Comparator<ScoreObjectId> CompareScore = new Comparator<ScoreObjectId>() {
        @Override
        public int compare(ScoreObjectId o1, ScoreObjectId o2) {
            return o1.getScore().compareTo(o2.getScore());
        }
    };
    private String name = "";
    private String objectId = "";
    private Integer Score = 0;

    public ScoreObjectId(String objectId, String name, Integer score) {
        this.objectId = objectId;
        this.name = name;
        this.Score = score;

    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return Score;
    }

    public void setScore(Integer score) {
        Score = score;
    }
}
class UserCredential {
    public static final Comparator<UserCredential> CompareSubmittedReviews = new Comparator<UserCredential>() {
        @Override
        public int compare(UserCredential o1, UserCredential o2) {
            return o1.getSubmittedReviews().compareTo(o2.getSubmittedReviews());
        }
    };

    public static final Comparator<UserCredential> CompareBoughtProducts = new Comparator<UserCredential>() {
        @Override
        public int compare(UserCredential o1, UserCredential o2) {
            return o1.getBoughtProducts().compareTo(o2.getBoughtProducts());
        }
    };
    private String objectId;
    private String email;
    private Integer submittedReviews;
    private Integer boughtProducts;

    public UserCredential(){}

    public UserCredential(String objectId, String email, Integer submittedReviews, Integer boughtProducts) {
        this.objectId = objectId;
        this.email = email;
        this.submittedReviews = submittedReviews;
        this.boughtProducts = boughtProducts;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getSubmittedReviews() {
        return submittedReviews;
    }

    public void setSubmittedReviews(Integer submittedReviews) {
        this.submittedReviews = submittedReviews;
    }

    public Integer getBoughtProducts() {
        return boughtProducts;
    }

    public void setBoughtProducts(Integer boughtProducts) {
        this.boughtProducts = boughtProducts;
    }
}
class ProfileInformation {
    public static final int MALE = 0;
    public static final int FEMALE = 1;

    private String objectId = "";
    private String name = "";
    private String lastName = "";
    private int sex = 0;
    private String userFK = "";
    private String phoneNumber = "";
    private Calendar dateOfBirth ;

    public ProfileInformation(){}

    public ProfileInformation(String objectId, String name, String lastName, int sex, String userFK, String phoneNumber, Calendar dateOfBirth) {
        this.objectId = objectId;
        this.name = name;
        this.lastName = lastName;
        this.sex = sex;
        this.userFK = userFK;
        this.phoneNumber = phoneNumber;
        this.dateOfBirth = dateOfBirth;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getUserFK() {
        return userFK;
    }

    public void setUserFK(String userFK) {
        this.userFK = userFK;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Calendar getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Calendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
class Category {

    public static final Comparator<Category> CompareScore = new Comparator<Category>() {
        @Override
        public int compare(Category o1, Category o2) {
            return o1.getScore().compareTo(o2.getScore());
        }
    };

    public static final Comparator<Category> CompareTotalIncome = new Comparator<Category>() {
        @Override
        public int compare(Category o1, Category o2) {
            return o1.getTotalIncome().compareTo(o2.getTotalIncome());
        }
    };

    private String objectId = "";
    private String title = "";
    private String parent = "";
    private Integer score = 0;
    private Double totalIncome = 0.0;

    public Category(){}

    public Category(Category category){
        this(category.getObjectId(), category.getTitle());
    }

    public Category(String objectId, String title) {
        this.objectId = objectId;
        this.title = title;
        this.score = 0;
    }

    public Category(String objectId, String title, Integer score) {
        this.objectId = objectId;
        this.title = title;
        this.score = score;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void incrementScore(Integer score){
        this.score+=score;
    }

    public void incrementScore(){
        this.score++;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public void incrementTotalIncome(Double totalIncome){
        setTotalIncome(getTotalIncome() + totalIncome);
    }

}
class Product {
    public static final Comparator<Product> CompareTotalIncome = new Comparator<Product>() {
        @Override
        public int compare(Product o1, Product o2) {
            return o1.getTotalIncome().compareTo(o2.getTotalIncome());
        }
    };

    public static final Comparator<Product> CompareTimesBought = new Comparator<Product>() {
        @Override
        public int compare(Product o1, Product o2) {
            return o1.getTimesBought().compareTo(o2.getTimesBought());
        }
    };

    public static final Comparator<Product> ComparePrice = new Comparator<Product>() {
        @Override
        public int compare(Product o1, Product o2) {
            return (o1.getPrice()).compareTo(o2.getPrice());
        }
    };

    public static final Comparator<Product> CompareAvgRating = new Comparator<Product>() {
        @Override
        public int compare(Product o1, Product o2) {
            return o1.getAvgRating().compareTo(o2.getAvgRating());
        }
    };

    private String objectId = "";
    private String name = "";
    private Integer timesBought = 0;
    private Double price = 0.0;
    private Double avgRating = 0.0;
    private String categoryFK = "";

    public Product(){};

    public Product(String objectId, String name, Integer timesBought, Double price, Double avgRating, String categoryFK) {
        this.objectId = objectId;
        this.name = name;
        this.timesBought = timesBought;
        this.price = price;
        this.avgRating = avgRating;
        this.categoryFK = categoryFK;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTimesBought() {
        return timesBought;
    }

    public void setTimesBought(Integer timesBought) {
        this.timesBought = timesBought;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Double avgRating) {
        this.avgRating = avgRating;
    }

    public String getCategoryFK() {
        return categoryFK;
    }

    public void setCategoryFK(String categoryFK) {
        this.categoryFK = categoryFK;
    }

    public Double getTotalIncome() {
        return price * timesBought;
    }

}



class Review {
    public static final Comparator<Review> CompareTotalIncome = new Comparator<Review>() {
        @Override
        public int compare(Review o1, Review o2) {
            return o1.getRating().compareTo(o2.getRating());
        }
    };

    private String objectId = "";
    private String userFK = "";
    private String productFK = "";
    private Integer rating = 0;

    public Review(){}

    public Review(String objectId, String userFK, String productFK, Integer rating) {
        this.objectId = objectId;
        this.userFK = userFK;
        this.productFK = productFK;
        this.rating = rating;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getUserFK() {
        return userFK;
    }

    public void setUserFK(String userFK) {
        this.userFK = userFK;
    }

    public String getProductFK() {
        return productFK;
    }

    public void setProductFK(String productFK) {
        this.productFK = productFK;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}

public class BackendInterface {
    private static final BackendInterface instance = new BackendInterface();
    private RestService rs = new RestService(new RestTemplateBuilder());
    private static final String URL = "https://parseapi.back4app.com";
    private static final String APP_ID = "wZuV0RvAbmAXIKAq1GeGLcJWZRHOa4PEFby8AHt4";
    private static final String REST_API_KEY = "qSa1fvEFuh3XlSX4kU7hjX18IaOXQV3thUaRIoNO";

    private class RestService {

        private final RestTemplate restTemplate;

        public RestService(RestTemplateBuilder restTemplateBuilder) {
            this.restTemplate = restTemplateBuilder.build();
        }

        public String getPlainJSON(String cmd, List<Map<String, String>> uriVariablesList) throws HttpStatusCodeException {
            String fullUrl = URL + cmd;
            // create headers
            HttpHeaders headers = new HttpHeaders();
            // set `accept` header
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            // set custom header
            headers.set("X-Parse-Application-Id", APP_ID);
            headers.set("X-Parse-REST-API-Key", REST_API_KEY);

            // transform url to add dynamically map elements ( uriVariables)
            Map<String, String> uriVariablesMap = new HashMap<>();
            if (uriVariablesList != null) {
                fullUrl += "?";
                for(int i =0; i < uriVariablesList.size(); i++){
                    if(i>0){
                        fullUrl+= "&";
                    }
                    Map<String, String> uriVariable = uriVariablesList.get(i);
                    String key = uriVariable.get("key");
                    String value = uriVariable.get("value");
                    fullUrl += key + "=" + "{" + key + "}";
                    uriVariablesMap.put(key, value);
                }
            }
            // build the request
            HttpEntity request = new HttpEntity(headers);

            // use `exchange` method for HTTP call
            ResponseEntity<String> response = this.restTemplate.exchange(fullUrl, HttpMethod.GET, request, String.class, uriVariablesMap);
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            } else {
                return null;
            }
        }
    }

    private BackendInterface() {
    }

    public static BackendInterface getInstance() {
        return instance;
    }

    private String getJsonField(JsonNode jsonNode, String fieldName){
        String result="";
        if(jsonNode != null && jsonNode.get(fieldName) != null)
            result =  jsonNode.get(fieldName).toString().replace("\"", "");
        return result;
    }

    private List<Map<String, String>> getStatistic(String url, List<Map<String, String>> urldataList, String keyName, String keyValue) throws DBException {
        List<Map<String, String>> dataset = new ArrayList<Map<String, String>>();
        try {
            RestService rs = new RestService(new RestTemplateBuilder());

            //create ObjectMapper instance
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode objectNode = objectMapper.createObjectNode();

            //read operator file into tree model
            JsonNode rootNode = objectMapper.readTree(rs.getPlainJSON(url, urldataList));
            JsonNode results = rootNode.get("results");

            if (results.isArray()) {
                Map<String, String> temp = null;

                String value = "";
                for (final JsonNode result : results) {
                    if (result.get(keyValue) != null) {
                        value = result.get(keyValue).toString();
                    } else {
                        value = "0";
                    }
                    temp = new HashMap<>();
                    temp.put("key", result.get(keyName).toString());
                    temp.put("value", value);
                    dataset.add(temp);
                }
            }
        } catch (IOException | HttpStatusCodeException ex) {
            if (ex instanceof HttpStatusCodeException) {
                if (((HttpStatusCodeException) ex).getRawStatusCode() >= 400) {
                    throw new DBException("Wrong Username", DBException.LOGIN_WRONG_USERNAME);
                }
            } else if (ex instanceof IOException) {
                ((IOException) ex).printStackTrace();
            }
        }
        return dataset;
    }


    public boolean checkLogin(String email, String password) throws DBException {
        boolean response = false;
        if (email.equals(""))
            email = "404";
        if (password.equals(""))
            password = null;

        try {
            RestService rs = new RestService(new RestTemplateBuilder());

            //create ObjectMapper instance
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, String> urlData = new HashMap<>();

            ObjectNode jsonWhereStatement = objectMapper.createObjectNode();
            jsonWhereStatement.put("email", email);

            List<Map<String, String>> urlDataList = new ArrayList<>();
            urlData.put("key", "where");
            urlData.put("value", jsonWhereStatement.toString());
            urlDataList.add(urlData);
            //read operator file into tree model
            JsonNode rootNode = objectMapper.readTree(rs.getPlainJSON("/classes/OperatorCredential", urlDataList));


            //read email
            // TODO: E' orribile ?
            String emailServer = rootNode.findPath("email").toString().replace("\"", "");
            if (!emailServer.equals(email)) {
                throw new DBException("Wrong Username", DBException.LOGIN_WRONG_USERNAME);
            }
            String passwordServer = rootNode.findPath("password").toString().replace("\"", "");
            if (passwordServer.equals(password)) {
                response = true;
            } else {
                throw new DBException("Wrong Password", DBException.LOGIN_WRONG_PASSWORD);
            }

        } catch (IOException | HttpStatusCodeException ex) {
            if (ex instanceof HttpStatusCodeException) {
                if (((HttpStatusCodeException) ex).getRawStatusCode() >= 400) {
                    throw new DBException("Wrong Username", DBException.CONNECTION_ERROR);
                }
            } else if (ex instanceof IOException) {
                ((IOException) ex).printStackTrace();
            }
        }
        return response;
    }

    /*--- Global Filters --- */

    /* Customers Statistics */
    public List<UserCredential> getUserCredential() {
        List<UserCredential> resultsList = new ArrayList<>();

        //read operator file into tree model
        JsonNode rootNode = null;
        try {
            rootNode = new ObjectMapper().readTree(rs.getPlainJSON("/classes/UserCredential/", null));
            JsonNode resultsJson = rootNode.get("results");

            if (resultsJson.isArray()) {
                UserCredential uc;
                for (final JsonNode result : resultsJson) {
                    if (result.get("objectId") != null) {
                        uc = new UserCredential();
                        uc.setObjectId(getJsonField(result, "objectId"));
                    if (result.get("email") != null) {
                        uc.setEmail(getJsonField(result, "email"));
                    }
                        if (result.get("boughtProducts") != null) {
                            uc.setBoughtProducts(Integer.parseInt(result.get("boughtProducts").toString()));
                        } else {
                            uc.setBoughtProducts(0);
                        }

                        if (result.get("submittedReviews") != null) {
                            uc.setSubmittedReviews(Integer.parseInt(result.get("submittedReviews").toString()));
                        } else {
                            uc.setBoughtProducts(0);
                        }
                        resultsList.add(uc);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultsList;
    }

    public List<UserCredential> getProfileInformation() {
        List<UserCredential> resultsList = new ArrayList<>();

        //read operator file into tree model
        JsonNode rootNode = null;
        try {
            rootNode = new ObjectMapper().readTree(rs.getPlainJSON("/classes/UserCredential/", null));
            JsonNode resultsJson = rootNode.get("results");

            if (resultsJson.isArray()) {
                UserCredential uc;
                for (final JsonNode result : resultsJson) {
                    if (result.get("email") != null) {
                        uc = new UserCredential();
                        uc.setEmail(result.get("email").toString());

                        if (result.get("boughtProducts") != null) {
                            uc.setBoughtProducts(Integer.parseInt(result.get("boughtProducts").toString()));
                        } else {
                            uc.setBoughtProducts(0);
                        }

                        if (result.get("submittedReviews") != null) {
                            uc.setSubmittedReviews(Integer.parseInt(result.get("submittedReviews").toString()));
                        } else {
                            uc.setBoughtProducts(0);
                        }
                        resultsList.add(uc);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultsList;
    }

    public List<Map<String, String>> getCustomerTopReviewers() throws DBException {
        List<Map<String, String>> urldataList = new ArrayList<>();
        Map<String, String> urlDataMap = new HashMap<>();
        urlDataMap.put("key", "order");
        urlDataMap.put("value", "-submittedReviews");
        urldataList.add(urlDataMap);
        urlDataMap = new HashMap<>();
        urlDataMap.put("key", "limit");
        urlDataMap.put("value", "5");
        urldataList.add(urlDataMap);
        return getStatistic("/classes/UserCredential/", urldataList, "email", "submittedReviews");
    }

    public List<Map<String, String>> getCustomerTopBuyers() throws DBException {
        List<Map<String, String>> urldataList = new ArrayList<>();
        Map<String, String> urlDataMap = new HashMap<>();
        urlDataMap.put("key", "order");
        urlDataMap.put("value", "-boughtProducts");
        urldataList.add(urlDataMap);
        urlDataMap = new HashMap<>();
        urlDataMap.put("key", "limit");
        urlDataMap.put("value", "5");
        urldataList.add(urlDataMap);
        return getStatistic("/classes/UserCredential/", urldataList, "email", "boughtProducts");
    }

    public List<Map<String, String>> getCustomerGenderStats() throws DBException {
        List<Map<String, String>> resultList = new ArrayList<>();

        List<Map<String, String>> temp = getStatistic("/classes/ProfileInformation/", null, "objectId", "sex");
        int male = 0, female = 0;
        for (int i = 0; i < temp.size(); i++) {
            if (Integer.parseInt(temp.get(i).get("value")) == 0) {
                male += 1;
            } else if (Integer.parseInt(temp.get(i).get("value")) == 1) {
                female += 1;
            }
        }

        Map<String, String> resultMap = new HashMap<String, String>();
        resultMap.put("key", "male");
        resultMap.put("value", Integer.toString(male));
        resultList.add(resultMap);

        resultMap = new HashMap<String, String>();
        resultMap.put("key", "female");
        resultMap.put("value", Integer.toString(female));
        resultList.add(resultMap);

        return resultList;
    }

    public List<Map<String, String>> getCustomerAvgAge() throws DBException {
        List<Map<String, String>> resultList = new ArrayList<>();
        List<Map<String, String>> temp = getStatistic("/classes/ProfileInformation/", null, "objectId", "dateOfBirth");
        try {
            JsonNode jsonDate;
            String dateString;
            int year = 0;
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);

            ObjectMapper om = new ObjectMapper();
            int year0To20 = 0, year21To40 = 0, year41To60 = 0, year60ToPlus = 0;

            for (int i = 0; i < temp.size(); i++) {
                jsonDate = om.readTree(temp.get(i).get("value").toString());
                dateString = getJsonField(jsonDate,"iso");
                year = Integer.parseInt(dateString.split("-")[0]);
                year = currentYear - year;

                if (year > 0 && year <= 20) {
                    year0To20++;
                } else if (year > 20 && year <= 40) {
                    year21To40++;
                } else if (year > 40 && year <= 60) {
                    year41To60++;
                } else if (year > 60) {
                    year60ToPlus++;
                }
            }

            Map<String, String> resultMap = new HashMap<String, String>();
            resultMap.put("key", "0-20");
            resultMap.put("value", Integer.toString(year0To20));
            resultList.add(resultMap);

            resultMap = new HashMap<String, String>();
            resultMap.put("key", "21-40");
            resultMap.put("value", Integer.toString(year21To40));
            resultList.add(resultMap);

            resultMap = new HashMap<String, String>();
            resultMap.put("key", "41-60");
            resultMap.put("value", Integer.toString(year41To60));
            resultList.add(resultMap);

            resultMap = new HashMap<String, String>();
            resultMap.put("key", "60+");
            resultMap.put("value", Integer.toString(year60ToPlus));
            resultList.add(resultMap);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return resultList;
    }

    /* Products Statistics */
    private List<Product> getProducts() throws DBException {
        List<Product> resultsList = new ArrayList<Product>();

        //read operator file into tree model
        JsonNode rootNode = null;
        try {
            rootNode = new ObjectMapper().readTree(rs.getPlainJSON("/classes/Product/", null));
            JsonNode resultsJson = rootNode.get("results");

            if (resultsJson.isArray()) {
                Product p;

                for (final JsonNode result : resultsJson) {
                    if (result.get("name") != null) {
                        p = new Product();
                        p.setName(getJsonField(result, "name"));

                        if(result.get("objectId") != null){
                            p.setObjectId(getJsonField(result, "objectId"));
                        }
                        if (result.get("price") != null) {
                            p.setPrice(Double.parseDouble(result.get("price").toString()));
                        }

                        if (result.get("timesBought") != null) {
                            p.setTimesBought(Integer.parseInt(result.get("timesBought").toString()));
                        }

                        if (result.get("avgRating") != null) {
                            p.setAvgRating(Double.parseDouble(result.get("avgRating").toString()));
                        }

                        if (result.get("category") != null) {
                            JsonNode jsonCategory = new ObjectMapper().readTree(result.get("category").toString());
                            p.setCategoryFK(getJsonField(jsonCategory, "objectId"));
                        }
                        resultsList.add(p);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultsList;
    }

    public List<Map<String, String>> getProductTopSellers() throws DBException {
        List<Map<String, String>> urldataList = new ArrayList<>();
        Map<String, String> urlDataMap = new HashMap<>();

        urlDataMap.put("key", "order");
        urlDataMap.put("value", "-timesBought");
        urldataList.add(urlDataMap);
        urlDataMap = new HashMap<>();
        urlDataMap.put("key", "limit");
        urlDataMap.put("value", "5");
        urldataList.add(urlDataMap);
        return getStatistic("/classes/Product/", urldataList, "name", "timesBought");
    }

    public List<Map<String, String>> getProductTopRated() throws DBException {
        List<Map<String, String>> urldataList = new ArrayList<>();
        Map<String, String> urlDataMap = new HashMap<>();

        urlDataMap.put("key", "order");
        urlDataMap.put("value", "-avgRating");
        urldataList.add(urlDataMap);
        urlDataMap = new HashMap<>();
        urlDataMap.put("key", "limit");
        urlDataMap.put("value", "5");
        urldataList.add(urlDataMap);
        return getStatistic("/classes/Product/", urldataList, "name", "avgRating");
    }

    public List<Map<String, String>> getProductTopIncome() throws DBException {
        List<Map<String, String>> resultsList = new ArrayList<>();
        List<Product> productsList = getProducts();

        Collections.sort(productsList, Collections.reverseOrder(Product.CompareTotalIncome));
        Map<String, String> resultsMap;

        for(int i=0; i < productsList.size(); i++){
            if (i == 5) {
                break;
            }
            Product p = productsList.get(i);
            resultsMap = new HashMap<>();
            resultsMap.put("key", p.getName());
            resultsMap.put("value", p.getTotalIncome().toString());
            resultsList.add(resultsMap);
        }

        return resultsList;
    }

    /* Categories Statistics */
    private List<Category> getCategories() throws DBException {
        List<Category> resultsList = new ArrayList<Category>();

        //read operator file into tree model
        JsonNode rootNode = null;
        try {
            rootNode = new ObjectMapper().readTree(rs.getPlainJSON("/classes/Category/", null));
            JsonNode resultsJson = rootNode.get("results");

            if (resultsJson.isArray()) {
                Category p;

                for (final JsonNode result : resultsJson) {
                    if (result.get("title") != null) {
                        p = new Category();
                        p.setTitle(result.get("title").toString());

                        if(result.get("objectId") != null) {
                            p.setObjectId(getJsonField(result, "objectId"));
                        }

                        if(result.get("parent") != null) {
                            p.setParent(getJsonField(result, "parent"));
                        }
                        resultsList.add(p);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultsList;
    }

    public List<Map<String, String>> getCategoryTopSeller() throws DBException {
        List<Map<String, String>> resultsList = new ArrayList<>();
        Map<String, String> resultsMap = new HashMap<>();

        List<Product> products = getProducts();
        List<Category> categories = getCategories();

        for (Product product : products ) {
            for (Category category: categories) {
                if(category.getObjectId().equals(product.getCategoryFK())){
                    category.incrementScore();
                }
            }
        }

        Collections.sort(categories, Collections.reverseOrder(Category.CompareScore));

        for(int i=0; i < categories.size(); i++){
            if(i==5) {
                break;
            }
            Category c = categories.get(i);
            resultsMap = new HashMap<>();
            resultsMap.put("key", c.getTitle());
            resultsMap.put("value", c.getScore().toString());
            resultsList.add(resultsMap);
        }
        return resultsList;
    }

    public List<Map<String, String>> getCategorySubTopSeller() throws DBException {
        List<Map<String, String>> resultsList = new ArrayList<>();
        Map<String, String> resultsMap = new HashMap<>();

        List<Product> products = getProducts();
        List<Category> categories = getCategories();

        for (Product product : products ) {
            for (int i=0; i < categories.size(); i++) {
                Category category = categories.get(i);
                if(category.getParent() == ""){
                    categories.remove(i);
                }else if(category.getObjectId().equals(product.getCategoryFK())){
                    category.incrementScore();
                }
            }
        }

        Collections.sort(categories, Collections.reverseOrder(Category.CompareScore));

        for(int i=0; i < categories.size(); i++){
            if(i==5) {
                break;
            }
            Category c = categories.get(i);
            resultsMap = new HashMap<>();
            resultsMap.put("key", c.getTitle());
            resultsMap.put("value", c.getScore().toString());
            resultsList.add(resultsMap);
        }
        return resultsList;
    }

    public List<Map<String, String>> getCategoryTopIncome() throws DBException {
        List<Map<String, String>> resultsList = new ArrayList<>();
        Map<String, String> resultsMap = new HashMap<>();

        List<Product> products = getProducts();
        List<Category> categories = getCategories();

        for (Product product : products ) {
            for (Category category: categories) {
                if(category.getObjectId().equals(product.getCategoryFK())){
                    category.incrementTotalIncome(product.getTotalIncome());
                }
            }
        }

        Collections.sort(categories, Collections.reverseOrder(Category.CompareTotalIncome));

        for(int i=0; i < categories.size(); i++){
            if(i==5) {
                break;
            }
            Category c = categories.get(i);
            resultsMap = new HashMap<>();
            resultsMap.put("key", c.getTitle());
            resultsMap.put("value", c.getTotalIncome().toString());
            resultsList.add(resultsMap);
        }
        return resultsList;
    }

    public List<Map<String, String>> getCategorySubTopIncome() throws DBException {
        List<Map<String, String>> resultsList = new ArrayList<>();
        Map<String, String> resultsMap = new HashMap<>();

        List<Product> products = getProducts();
        List<Category> categories = getCategories();

        for (Product product : products ) {
            for (int i=0; i < categories.size(); i++) {
                Category category = categories.get(i);
                if(category.getParent() == ""){
                    categories.remove(i);
                }else if(category.getObjectId().equals(product.getCategoryFK())){
                    category.incrementTotalIncome(product.getTotalIncome());
                }
            }
        }

        Collections.sort(categories, Collections.reverseOrder(Category.CompareTotalIncome));

        for(int i=0; i < categories.size(); i++){
            if(i==5) {
                break;
            }
            Category c = categories.get(i);
            resultsMap = new HashMap<>();
            resultsMap.put("key", c.getTitle());
            resultsMap.put("value", c.getTotalIncome().toString());
            resultsList.add(resultsMap);
        }
        return resultsList;
    }


    /* --- Specific Filters ---*/
    public  List<Map<String, String>> getItemsByString(String url, String columnToSearch, String queryString ){
        List<Map<String, String>> resultsList = new ArrayList<Map<String, String>>();

        //read operator file into tree model

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> urlData = new HashMap<>();

        ObjectNode jsonWhereStatement = objectMapper.createObjectNode();
        ObjectNode jsonRegexStatement = objectMapper.createObjectNode();
        jsonRegexStatement.put("$regex", "(?i).*" + queryString + ".*");
        jsonWhereStatement.put(columnToSearch,jsonRegexStatement.toString());

        List<Map<String, String>> urlDataList = new ArrayList<>();
        urlData.put("key", "where");
        urlData.put("value", jsonWhereStatement.toString().replace("\"{", "{").replace("}\"", "}").replaceAll("\\\\","" ));
        urlDataList.add(urlData);

        JsonNode rootNode;
        try {
            rootNode = new ObjectMapper().readTree(rs.getPlainJSON(url, urlDataList));
            JsonNode resultsJson = rootNode.get("results");

            if (resultsJson.isArray()) {
                Map<String, String> p;

                for (final JsonNode result : resultsJson) {
                    if (result.get(columnToSearch) != null) {
                        p = new HashMap<>();
                        p.put("name", getJsonField(result, columnToSearch));

                        if (result.get("objectId") != null) {
                            p.put("objectId", getJsonField(result, "objectId"));
                        }
                        resultsList.add(p);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultsList;
    }

    public  Map<String,String> getItemByObjectId(String url, String objectId){
        RestService rs = new RestService(new RestTemplateBuilder());

        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, String> urlData = new HashMap<>();
        Map<String, String> resultMap = new HashMap<>();

        ObjectNode jsonWhereStatement = objectMapper.createObjectNode();
        jsonWhereStatement.put("objectId", objectId);

        List<Map<String, String>> urlDataList = new ArrayList<>();
        urlData.put("key", "where");
        urlData.put("value", jsonWhereStatement.toString());
        urlDataList.add(urlData);
        //read operator file into tree model
        try {
            JsonNode rootNode = objectMapper.readTree(rs.getPlainJSON(url, urlDataList));
            JsonNode resultsJson = rootNode.get("results");

            if (resultsJson.isArray()) {
                for (final JsonNode result : resultsJson) {
                    if(result.get("objectId") != null) {
                        String resultObjId = getJsonField(result, "objectId");
                        resultMap.put("objectId", resultObjId );
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resultMap;
    }

    public List<Map<String, String>> getCustomersByString(String stringQuery){
        List<Map<String, String>> resultsList = new ArrayList<Map<String, String>>();
        resultsList = getItemsByString("/classes/UserCredential", "email", stringQuery);
        return  resultsList;
    }

    public List<Map<String, String>> getProductsByString(String stringQuery){
        List<Map<String, String>> resultsList = new ArrayList<Map<String, String>>();
        resultsList = getItemsByString("/classes/Product", "name", stringQuery);
        return  resultsList;
    }

    public List<Map<String, String>> getCategoryByString(String stringQuery){
        List<Map<String, String>> resultsList = new ArrayList<Map<String, String>>();
        resultsList = getItemsByString("/classes/Category", "title", stringQuery);
        return  resultsList;
    }

    /* Customers Statistics */
    private List<Review> getReviews() throws DBException {
        List<Review> resultsList = new ArrayList<>();

        //read operator file into tree model
        JsonNode rootNode = null;
        try {
            rootNode = new ObjectMapper().readTree(rs.getPlainJSON("/classes/Review/", null));
            JsonNode resultsJson = rootNode.get("results");

            if (resultsJson.isArray()) {
                Review r;

                for (final JsonNode result : resultsJson) {
                    if (result.get("objectId") != null) {
                        r = new Review();
                        r.setObjectId(getJsonField(result, "objectId"));

                        if (result.get("rating") != null) {
                            System.out.println(result.get("rating"));
                            r.setRating(Integer.parseInt(result.get("rating").toString()));
                        }

                        if (result.get("user") != null) {
                            JsonNode jsonCategory = new ObjectMapper().readTree(result.get("user").toString());
                            r.setUserFK(getJsonField(jsonCategory, "objectId"));
                        }
                        if (result.get("product") != null) {
                            JsonNode jsonCategory = new ObjectMapper().readTree(result.get("product").toString());
                            r.setProductFK(getJsonField(jsonCategory, "objectId"));
                        }

                        resultsList.add(r);
                    }
                }
            }
    }catch (IOException e) {
            e.printStackTrace();
        }
        return resultsList;
    }
    public List<Map<String,String>> getReviewNumber(List<Map<String,String>> objectIds){

        List<Map<String,String>> resultsList = new ArrayList<>();
        List<Review> reviews;
        List<ScoreObjectId>  scoreObjectIds= new ArrayList<>();

        try {
            reviews = getReviews();
            for (Map<String,String> objectId: objectIds) {
                ScoreObjectId scoreObjectId = new ScoreObjectId(objectId.get("objectId"),objectId.get("name"),0 );
                scoreObjectIds.add(scoreObjectId);
                for (Review review : reviews) {
                    if(review.getUserFK().equals(objectId.get("objectId"))){
                        scoreObjectId.setScore(scoreObjectId.getScore()+1);
                    }
                }
            }

            Collections.sort(scoreObjectIds,Collections.reverseOrder(ScoreObjectId.CompareScore));

            for (ScoreObjectId scoreObjectId : scoreObjectIds) {
                Map<String,String> resultMap = new HashMap<>();
                resultMap.put("key", scoreObjectId.getName());
                resultMap.put("value", scoreObjectId.getScore().toString());
                resultsList.add(resultMap);
            }
        } catch (DBException e) {
            e.printStackTrace();
        }
        return resultsList;
    }

    public List<Map<String,String>> getBoughtProducts(List<Map<String,String>> objectIds){

        List<Map<String,String>> resultsList = new ArrayList<>();
        List<Product> products;
        Map<String,String> resultMap;
        List<ScoreObjectId>  scoreObjectIds= new ArrayList<>();

        try {
            products = getProducts();
        for (Map<String,String> objectId: objectIds) {
            ScoreObjectId scoreObjectId = new ScoreObjectId(objectId.get("objectId"),objectId.get("name"),0 );
            scoreObjectIds.add(scoreObjectId);
            for (Product product : products) {
                if(product.getObjectId().equals(objectId.get("objectId"))){
                    resultMap = new HashMap<>();
                    resultMap.put("key", product.getName());
                    resultMap.put("key", product.getTimesBought().toString());
                    resultsList.add(resultMap);
                }
            }

        }
        } catch (DBException e) {
            e.printStackTrace();
        }
        return resultsList;
    }

    /* Products Statistics */

    public List<Map<String,String>> getTopProducts(List<Map<String,String>> objectIds){

        List<Map<String,String>> resultsList = new ArrayList<>();
        List<Product> products;
        try {
            products = getProducts();

        for (Map<String,String> objectId: objectIds) {
            for (Product product : products) {
                if(product.getObjectId().equals(objectId.get("objectId"))){
                    Map<String,String> resultMap = new HashMap<>();
                    resultMap.put("key", product.getName());
                    resultMap.put("value", product.getTimesBought().toString());
                    resultsList.add(resultMap);
                }
            }
        }
        } catch (DBException e) {
            e.printStackTrace();
        }

        return resultsList;
    }
    public List<Map<String,String>> getTopRatedProducts(List<Map<String,String>> objectIds){

        List<Map<String,String>> resultsList = new ArrayList<>();
        List<Product> products;
        try {
            products = getProducts();

            for (Map<String,String> objectId: objectIds) {
                for (Product product : products) {
                    if(product.getObjectId().equals(objectId.get("objectId"))){
                        Map<String,String> resultMap = new HashMap<>();
                        resultMap.put("key", product.getName());
                        resultMap.put("value", product.getAvgRating().toString());
                        resultsList.add(resultMap);
                    }
                }
            }
        } catch (DBException e) {
            e.printStackTrace();
        }

        return resultsList;
    }
    public List<Map<String,String>> getTopIncomeProducts(List<Map<String,String>> objectIds){

        List<Map<String,String>> resultsList = new ArrayList<>();
        List<Product> products;
        try {
            products = getProducts();

            for (Map<String,String> objectId: objectIds) {
                for (Product product : products) {
                    if(product.getObjectId().equals(objectId.get("objectId"))){
                        Map<String,String> resultMap = new HashMap<>();
                        resultMap.put("key", product.getName());
                        resultMap.put("value", product.getTotalIncome().toString());
                        resultsList.add(resultMap);
                    }
                }
            }
        } catch (DBException e) {
            e.printStackTrace();
        }

        return resultsList;
    }

    /* Category Statistics */
    public List<Map<String,String>> getTopCategory(List<Map<String,String>> objectIds){
        List<Map<String, String>> resultsList = new ArrayList<>();
        Map<String, String> resultsMap;

        List<Product> products;
        List<Category> categories;
        try {
            products = getProducts();
            categories = getCategories();

            for (Product product : products ) {
                for (Category category: categories) {
                    if(category.getObjectId().equals(product.getCategoryFK())){
                        category.incrementScore();
                    }
                }
            }

            for (Category category: categories) {
                for (Map<String,String> objectId : objectIds) {
                    if (category.getObjectId().equals(objectId.get("objectId"))) {
                        resultsMap = new HashMap<>();
                        resultsMap.put("key",category.getTitle());
                        resultsMap.put("value", category.getScore().toString());
                        resultsList.add(resultsMap);
                    }
                }
            }
        } catch (DBException e) {
            e.printStackTrace();
        }
        return resultsList;
    }

    public List<Map<String,String>> getTopIncomeCategory(List<Map<String,String>> objectIds){
        List<Map<String, String>> resultsList = new ArrayList<>();
        Map<String, String> resultsMap;

        List<Product> products;
        List<Category> categories;
        try {
            products = getProducts();
            categories = getCategories();

            for (Product product : products ) {
                for (Category category: categories) {
                    if(category.getObjectId().equals(product.getCategoryFK())){
                        category.incrementTotalIncome(product.getTotalIncome());
                    }
                }
            }

            for (Category category: categories) {
                for (Map<String,String> objectId : objectIds) {
                    if (category.getObjectId().equals(objectId.get("objectId"))) {
                        resultsMap = new HashMap<>();
                        resultsMap.put("key",category.getTitle());
                        resultsMap.put("value", category.getTotalIncome().toString());
                        resultsList.add(resultsMap);
                    }
                }
            }
        } catch (DBException e) {
            e.printStackTrace();
        }
        return resultsList;
    }


}

