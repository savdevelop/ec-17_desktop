import ec17.desktop.boundary.HomePage;
import ec17.desktop.boundary.LoginPage;
import ec17.desktop.boundary.StatisticsPage;
import ec17.desktop.control.HomePageController;
import ec17.desktop.control.LoginController;
import ec17.desktop.control.StatisticsController;

import javax.swing.*;

public class Application {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                /* Create Boundaries */
                LoginPage lp = new LoginPage();
                HomePage hp = new HomePage();
                StatisticsPage sp = new StatisticsPage();
                /* Create Entities */
                /* NONE */

                /* Creates Controllers */
                StatisticsController sc = new StatisticsController(sp);
                HomePageController hpc = new HomePageController(hp, sc);
                LoginController lc = new LoginController(lp, hpc);

                /* Initialize the ecosystem */
                hpc.setLoginController(lc);
                lc.activate();
            }
        });
    }
}
